/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v02;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class Account
{
    private int account_id;
    private LocalDate date_opened;
    
    public  Account()
    {
        
    }
    
    public	LocalDate	getDateCreated()
    {
            return date_opened;
    }
    
    public  void    setDateCreated( LocalDate date )
    {
        date_opened = date;
    }
    
    public  void    setDateCreated( String date )
    {
        date_opened = LocalDate.parse(date);
    }

    public	int	getAccountId()
    {
            return account_id;
    }
}

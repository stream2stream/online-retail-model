/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v10;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public class Account    implements  Serializable
{
    private int account_id;
    private LocalDate date_opened = LocalDate.now();
    private HashMap<Integer, Order> orders = new HashMap<>();
    private Customer    itsCustomer;
    private transient   DisplayHandler  itsDisplayHandler;
    
    public  class   DisplayHandler implements BusinessObjectRenderer
    {
        @Override
        public  void    display()
        {
            System.out.println("Account\tACCOUNT ID\tLDATE CREATED");
            System.out.printf("\t%d\t\t%s\n", account_id, date_opened.toString());
        }
    }

    public  Account( Customer cc, int accId )
    {
        this.itsDisplayHandler = new DisplayHandler();
        account_id = accId;
        itsCustomer = cc;
    }
    
    public  Account()
    {
         this.itsDisplayHandler = new DisplayHandler();       
    }
    
    public  BusinessObjectRenderer  getRenderer()
    {
        if( itsDisplayHandler == null )
            itsDisplayHandler = new DisplayHandler();
        return itsDisplayHandler;
    }
    
    public Customer getCustomer()
    {
        return itsCustomer;
    }

    public void setCustomer(Customer pCustomer)
    {
        this.itsCustomer = pCustomer;
    }
    
    public	void	addOrder( Order anOrder )
    {
        orders.put(anOrder.getOrderNo(), anOrder );
    }
    
    public	HashMap<Integer, Order> getOrders()
    {
        return orders;
    }

    public	LocalDate	getDateCreated()
    {
            return date_opened;
    }
    
    public  void    setDateCreated( LocalDate date )
    {
        date_opened = date;
    }
    
    public  void    setDateCreated( String date )
    {
        date_opened = LocalDate.parse(date);
    }

    public	int	getAccountId()
    {
            return account_id;
    }
}

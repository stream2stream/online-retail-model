/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

import java.util.HashMap;


public class Warehouse
{

    private static Warehouse self;
    private HashMap< String, Product> products = new HashMap<>();

    private Warehouse()
    {
    }

    public static   Warehouse getInstance()
    {
        if (self == null)
        {
            self = new Warehouse();
            self.addProducts();
        }
        return self;
    }

    private void addProducts()
    {
        // Create a few Products
        products.put("JDF-001", new Product("JVC DVD Player", 179.99, "JDF-001", 5));
        products.put("SMA-001", new Product("Sony MIDI Amplifier", 203.45, "SMA-001", 3));
        products.put("LAIO-302", new Product("Lenovo All In One 302", 312.00, "LAIO-302", 7));

        /*
         * No need to create Item objects here, the new version of the Product class
         * has a method called purchaseItem( qty ) that returns an Item object
         * that is then added to the Order object
        */
    }

    public  HashMap<String, Product>    getProducts()
    {
        return this.products;
    }
    
    public  Product getProduct( String pid )
    {
        return products.get( pid );
    }
}

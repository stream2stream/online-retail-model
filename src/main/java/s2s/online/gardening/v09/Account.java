/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public class Account    implements  Serializable
{
    private int account_id;
    private LocalDate date_opened = LocalDate.now();
    private HashMap<Integer, Order> orders = new HashMap<>();
    private Customer    itsCustomer;

    public  Account( Customer cc, int accId )
    {
        itsCustomer = cc;
        account_id = accId;
    }
    
    public  Account()
    {
        
    }
    
    public Customer getCustomer()
    {
        return itsCustomer;
    }

    public void setCustomer(Customer pCustomer)
    {
        this.itsCustomer = pCustomer;
    }
    
    public	void	addOrder( Order anOrder )
    {
        orders.put(anOrder.getOrderNo(), anOrder );
    }
    
    public	HashMap<Integer, Order> getOrders()
    {
        return orders;
    }

    public	LocalDate	getDateCreated()
    {
            return date_opened;
    }
    
    public  void    setDateCreated( LocalDate date )
    {
        date_opened = date;
    }
    
    public  void    setDateCreated( String date )
    {
        date_opened = LocalDate.parse(date);
    }

    public	int	getAccountId()
    {
            return account_id;
    }
}

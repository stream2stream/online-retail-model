/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v05;

/**
 *
 * @author Selvyn
 */
public class Item
{
    private double  item_cost;
    private int     quantity;
    private int     unique_id;
    private Product itsProduct;

    public Item(Product prd, double item_cost, int unique_id)
    {
        this.item_cost = item_cost;
        this.unique_id = unique_id;
        this.itsProduct = prd;
    }
    
    public  Item()
    {
    }
    
    public  Product getProduct()
    {
        return itsProduct;
    }
    
    public  double  getItemCost()
    {
        return item_cost;
    }
    
    public  int getQuantity()
    {
        return quantity;
    }
    
    public  int getUniqueId()
    {
        return unique_id;
    }
    
    public  double  getTotalCost()
    {
        return quantity * item_cost;
    }

    public void setItem_cost(double item_cost)
    {
        this.item_cost = item_cost;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public void setUnique_id(int unique_id)
    {
        this.unique_id = unique_id;
    }


}

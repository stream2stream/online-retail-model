/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

/**
 *
 * @author Selvyn
 */
public class XMLFormatter   implements StreamFormatter
{
    private StringBuilder   formattedStream;

    @Override
    public  String  format( Account acc )
    {
        formattedStream = new StringBuilder();
        
        formattedStream.append("<account>");
        formattedStream.append("\n\t<datecreated>");
        formattedStream.append(acc.getDateCreated().toString());
        formattedStream.append("</datecreated>");
        formattedStream.append("\n</account>");
        
        return formattedStream.toString();
    }

    @Override
    public  String  format( Customer cust )
    {
        formattedStream = new StringBuilder();
        
        formattedStream.append("<customer>");
        formattedStream.append("\n\t<fname>"); 
        formattedStream.append(cust.getFname()); 
        formattedStream.append("</fname>");
        formattedStream.append("\n\t<fname>");
        formattedStream.append(cust.getLname());
        formattedStream.append("</fname>");
        formattedStream.append("\n\t<fname>");
        formattedStream.append(cust.getDob());
        formattedStream.append("</fname>");
        formattedStream.append("\n\t<fname>");
        formattedStream.append(cust.getAddress());
        formattedStream.append("</fname>");
        formattedStream.append("\n</customer>");
        
        return formattedStream.toString();
    }
}

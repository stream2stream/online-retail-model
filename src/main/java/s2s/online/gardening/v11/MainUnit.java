/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v11;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
    public  static  void    main( String args[] )
    {
        ShutdownHandler sdh = new ShutdownHandler();
        
        KeyboardHandler kh = KeyboardHandler.getInstance();
        
        kh.addCommand("save", ()->{
            System.out.println("Save command called");
        });
        
        kh.addCommand("load", ()->{
            System.out.println("Load command called");
        });
        
        // This sdhould eb the last command we call as it is a blocking call
        sdh.waitForShutdownCommand();
    }
}

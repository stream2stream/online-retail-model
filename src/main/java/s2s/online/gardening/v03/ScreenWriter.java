/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v03;

/**
 *
 * @author Selvyn
 */
public class ScreenWriter
{
    public  void    writeAccount( Account acc )
    {
        System.out.printf("{\"Account\":[\n\t{\"Date Created\":\"%s\"}\n]}", acc.getDateCreated().toString() );
    }
    
    public  void    writeCustomer( Customer cc )
    {
        System.out.printf("{\"Customer\":[\n\t{\"First Name\":\"%s\"},", cc.getFname());
        System.out.printf("\n\t{\"Last Name\":\"%s\"},", cc.getLname());
        System.out.printf("\n\t{\"Date of Birth\":\"%s\"},", cc.getDob().toString());
        System.out.printf("\n\t{\"Address\":\"%s\"}", cc.getAddress());
        System.out.println("\n]}");
    }
}

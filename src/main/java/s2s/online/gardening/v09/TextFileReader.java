/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class TextFileReader implements  InputChannel
{
    private String  itsFileName;
    private BufferedReader itsFileReader;
    
    public  TextFileReader()
    {
    }
    
    @Override
    public  void    openForReading( String fname )
    {
        itsFileName = fname;
        
        try
        {
            itsFileReader = new BufferedReader(new FileReader(fname));
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    @Override
    public  String    read()
    {
        StringBuilder sb = new StringBuilder();
        String line;
        try
        {
            while( (line = itsFileReader.readLine()) != null )
                sb.append(line );
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            close();
        }
        return sb.toString();
    }   
    
    @Override
    public  void    close()
    {
        try
        {
            itsFileReader.close();
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

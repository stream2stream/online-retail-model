/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v10;

import java.io.Serializable;

/**
 *
 * @author Selvyn
 */
public class Item   implements Serializable
{
    private double  item_cost;
    private int     unique_id;
    private Product itsProduct;
    private int     quantity;
    private Order   itsOrder;

    public Item(Product theProduct, double item_cost, int qty, int unique_id)
    {
        itsProduct = theProduct;
        this.item_cost = item_cost;
        this.unique_id = unique_id;
        this.quantity = qty;
    }
    
    public  Item()
    {
    }
    
    public  Product getProduct()
    {
        return itsProduct;
    }
    
    public  Order   getOrder()
    {
        return itsOrder;
    }
    
    public  double  getItemCost()
    {
        return item_cost;
    }
    
    public  int getUniqueId()
    {
        return unique_id;
    }
    
    public  double  getTotalCost()
    {
        return quantity * item_cost;
    }

    public void setItem_cost(double item_cost)
    {
        this.item_cost = item_cost;
    }

    public void setUnique_id(int unique_id)
    {
        this.unique_id = unique_id;
    }

    public  int getQuantity()
    {
        return quantity;
    }
    
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }
    
    public  void    setOrder( Order ord )
    {
        itsOrder = ord;
    }
}

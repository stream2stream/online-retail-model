/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v02;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class Order
{
    private int order_no;
    private double total_cost;
    private LocalDate order_date;

    public Order()
    {
    }

    public int getOrderNo()
    {
        return order_no;
    }

    public void setOrderNo(int orderno)
    {
        order_no = orderno;
    }

    public LocalDate getDateCreated()
    {
        return order_date;
    }

    public void setDateCreated(LocalDate date)
    {
        order_date = date;
    }

    public void setDateCreated(String date)
    {
        order_date = LocalDate.parse(date);
    }

    public double getTotalCost()
    {
        double result = 0.0;

        return result;
    }
}

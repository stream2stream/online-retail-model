/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v08;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class Account
{
    private int account_id;
    private Customer itsCustomer;
    private LocalDate date_opened = LocalDate.now();
    private ArrayList orders = new ArrayList(10);
    
    public  Account( Customer cc, int accId )
    {
        itsCustomer = cc;
        account_id = accId;
    }
    
    public  Account()
    {
        
    }

    public Customer getCustomer()
    {
        return itsCustomer;
    }

    public void setCustomer(Customer itsCustomer)
    {
        this.itsCustomer = itsCustomer;
    }
    
    public	void	addOrder( Order anOrder )
    {
        orders.add( anOrder );
    }
    
    public	ArrayList	getOrders()
    {
        return orders;
    }

    public	LocalDate	getDateCreated()
    {
            return date_opened;
    }
    
    public  void    setDateCreated( LocalDate date )
    {
        date_opened = date;
    }
    
    public  void    setDateCreated( String date )
    {
        date_opened = LocalDate.parse(date);
    }

    public	int	getAccountId()
    {
            return account_id;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v12;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class SerializableFileWriter   implements  OutputChannel
{
    private String  itsFileName;
    private ObjectOutputStream itsOutputStream;
    
    public  SerializableFileWriter()
    {
    }
    
    @Override
    public  void    openForWriting( String fname )
    {
        itsFileName = fname;
        
        try
        {
            itsOutputStream = new ObjectOutputStream(new FileOutputStream(fname));
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @Override
    public  void    write( Object theObject )
    {
        try
        {
            itsOutputStream.writeObject(theObject);
        } catch (IOException ex)
        {
            Logger.getLogger(SerializableFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    
    
    @Override
    public  void    close()
    {
        try
        {
            itsOutputStream.close();
        } catch (IOException ex)
        {
            Logger.getLogger(SerializableFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public class CustomerAccountMgr
{
    private static CustomerAccountMgr self;
    private final HashMap< String, Customer> customers = new HashMap<>();
    private final HashMap< Integer, Account> accounts = new HashMap<>();

    public static   CustomerAccountMgr getInstance()
    {
        if (self == null)
        {
            self = new CustomerAccountMgr();
        }
        return self;
    }

    public  HashMap<String, Customer>    getCustomers()
    {
        return this.customers;
    }
    
    public  HashMap<Integer, Account>    getAccounts()
    {
        return this.accounts;
    }
    
    public  Customer getCustomer( String cid )
    {
        return customers.get( cid );
    }
    
    public  Account getAccount( int accId )
    {
        return accounts.get( accId );
    }
    
    public  void    addCustomer( String cid, Customer cc )
    {
        customers.put(cid, cc);
    }
    
    public  void    addAccount( Account acc )
    {
        accounts.put(acc.getAccountId(), acc);
    }
}

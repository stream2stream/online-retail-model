/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v05;

/**
 *
 * @author Selvyn
 */
public class XMLFormatter   extends StreamFormatter
{
    public  String  format( Account acc )
    {
        String result = "<account>";
            result += "\n\t<datecreated>";
            result += acc.getDateCreated().toString();
            result += "</datecreated>";
            result += "\n</account>";
        
        return result;
    }

    public  String  format( Customer cust )
    {
        String result = "<customer>";
            result += "\n\t<fname>" + cust.getFname() + "</fname>";
            result += "\n\t<fname>" + cust.getLname() + "</fname>";
            result += "\n\t<fname>" + cust.getDob() + "</fname>";
            result += "\n\t<fname>" + cust.getAddress() + "</fname>";
            result += "\n</customer>";
        
        return result;
    }
}

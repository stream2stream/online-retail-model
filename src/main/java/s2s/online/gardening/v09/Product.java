/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public class Product    implements Serializable
{
    private String  description;
    private double  price;
    private String  product_code;
    private int     quantity;
    private HashMap<Integer, Item> items = new HashMap<>();

    public  Product()
    {
        
    }
    
    public Product(String description, double price, String product_code, int qty)
    {
        this.description = description;
        this.price = price;
        this.product_code = product_code;
        this.quantity = qty;
    }
    
    public  void    decreaseStock() throws Exception
    {
        if( quantity < 1 )
            throw new NotEnoughStock(description + " is out of stock");
        
        quantity--;
    }
    
    public  void    decreaseStock(int amt) throws NotEnoughStock
    {
        if( quantity - amt < 0 )
            throw new NotEnoughStock(description + " is out of stock");
        quantity -= amt;
    }
    
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public String getProductCode()
    {
        return product_code;
    }

    public void setProductCode(String product_code)
    {
        this.product_code = product_code;
    }
    
    public  int getQuantity()
    {
        return quantity;
    }
    
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public  void    addItem( Item newItem )
    {
        items.put(newItem.getUniqueId(), newItem);
    }

    public  Item    getItem( int itemId )
    {
        return items.get(itemId );
    }
    
    public  HashMap< Integer, Item >    getItems()
    {
        return  items;
    }
    
    public  Item    purchaseItem( int qty ) throws InvalidPurchase, NotEnoughStock
    {
        this.decreaseStock(qty);
        
        return new Item(this, this.price, qty, 1);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v08;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{

    public static void main(String[] args)
    {
        try
        {
            Account acc = new Account(null, 1);

            // working with a ScreenWriter
            OutputChannel sw = Writer.getScreenWriter();

            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(acc));
            System.out.println("");

            sw.write(FormatFactory.getFormatter(WriterMode.XML_WRITER).format(acc));
            System.out.println("");

            Customer cc = new Customer("Selvyn", "Wright", LocalDate.of(1965, 5, 8), "Birmingham, UK");

            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(cc));

            System.out.println("");
            
            //Working with a TextFileWriter
            sw = Writer.getTextFileWriter("c:\\tmp\\account.json");
            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(acc));
            sw.close();
            sw = Writer.getTextFileWriter("c:\\tmp\\account.xml");
            sw.write(FormatFactory.getFormatter(WriterMode.XML_WRITER).format(acc));
            sw.close();
            sw = Writer.getTextFileWriter("c:\\tmp\\customer.json");
            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(cc));
            sw.close();
            
            // Read the files back in
            System.out.println( "===========================================" );
            TextFileReader tfr = new TextFileReader();
            tfr.openForReading("c:\\tmp\\account.json");
            String jsonStream = tfr.read();
            System.out.println( jsonStream );
            
            tfr.openForReading("c:\\tmp\\account.xml");
            String xmlStream = tfr.read();
            System.out.println( xmlStream );
            
            tfr.openForReading("c:\\tmp\\customer.json");
            jsonStream = tfr.read();
            System.out.println( jsonStream );

        } catch (Exception ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

/**
 *
 * @author Selvyn
 */
public class Writer
{
    private static  final   ScreenWriter    screenWriter = new ScreenWriter();
    private static  final   TextFileWriter  textFileWriter = new TextFileWriter();
    private static  final   SerializableFileWriter  serializableWriter = new SerializableFileWriter();
    
    public  static  OutputChannel    getScreenWriter()
    {
        return screenWriter;  
    }
    
    public  static  OutputChannel    getTextFileWriter( String fname )
    {
        textFileWriter.openForWriting(fname);
        
        return textFileWriter;  
    }
    
    public  static  SerializableFileWriter    getSerializableFileWriter( String fname )
    {
        serializableWriter.openForWriting(fname);
        
        return serializableWriter;  
    }
    
    public  static  Writer  getWriter( WriterMode mode )
    {
        Writer result = null;
        
        if( mode == WriterMode.XML_WRITER )
            result = null;
        
        return result;
    }
    
}

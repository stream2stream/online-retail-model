/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Selvyn
 */
public class KeyboardHandler implements Runnable
{
    public  final   Object itsMonitor = new Object();
    private static  KeyboardHandler self = null;
    private final   HashMap<String, CommandExecutor> commands = new HashMap<>();
    private final   String  stdPrompt = "\ntype quit to exit: ";
    private final   String  quitCmd = "quit";
    private static  final   StringBuilder promptCommands = new StringBuilder();
    
    public  static  KeyboardHandler  getInstance()
    {
        if( self == null )
            self = new KeyboardHandler();

        return self;
    }
    
    public	String	readString()
    {
        String result = null;
        InputStreamReader isr = new InputStreamReader( System.in );
        BufferedReader br = new BufferedReader( isr );
        
        try
        {
            result = br.readLine();
        }
        catch (IOException e)
        {
        }
        
        return result;
    }

    @Override
    public void run() 
    {
        String  textInput = "";
        self.addCommand("help", ()->
        {
            System.out.println("Available commands:");
            System.out.println( promptCommands );
        });
        for( Map.Entry<String, CommandExecutor> entry: self.commands.entrySet())
        {
           promptCommands.append("\t").append(entry.getKey()).append("\n");
        }
       
        while( ! textInput.equalsIgnoreCase(quitCmd))
        {
            System.out.print(stdPrompt);
            textInput = readString();
            CommandExecutor cmd = commands.get(textInput);
            if(cmd != null )
                cmd.execute();
        }
        synchronized( itsMonitor )
        {
            itsMonitor.notify();
        }
    }
    
    public  void    addCommand( String cmdId, CommandExecutor executor )
    {
        commands.put(cmdId, executor);
    }
}

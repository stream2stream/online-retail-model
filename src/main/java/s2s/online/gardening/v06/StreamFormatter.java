/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v06;

/**
 *
 * @author Selvyn
 */
public abstract class StreamFormatter
{    
    private StringBuilder   formattedStream = null;
    
    protected void    reset()
    {
        formattedStream = new StringBuilder();
    }
    
    protected void    append( String data )
    {
        formattedStream.append(data);
    }
    
    protected   String  getStreamAsString()
    {
        return formattedStream.toString();
    }
    
    public  abstract   String  format( Account acc );

    public  abstract   String  format( Customer acc );
}

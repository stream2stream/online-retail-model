/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author Selvyn
 */
public class Customer    implements  Serializable
{
    private LocalDate   dob;
    private String      address;
    private String      fname;
    private String      lname;
    private Account     itsAccount;
    
    public Customer()
    {
    }

    public Customer(String fname, String lname, LocalDate dob, String address)
    {
        this.dob = dob;
        this.address = address;
        this.fname = fname;
        this.lname = lname;
    }

    public  void    setAccount( Account acc )
    {
        itsAccount = acc;
    }
    
    public  Account getAccount()
    {
        return itsAccount;
    }
    
    public LocalDate getDob()
    {
        return dob;
    }

    public void setDob(LocalDate dob)
    {
        this.dob = dob;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getFname()
    {
        return fname;
    }

    public void setFname(String fname)
    {
        this.fname = fname;
    }

    public String getLname()
    {
        return lname;
    }

    public void setLname(String lname)
    {
        this.lname = lname;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v08;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class TextFileWriter   implements  OutputChannel
{
    private PrintWriter itsPrintWriter;
    
    public  void    openForWriting( String fname )
    {
        try
        {
            itsPrintWriter = new PrintWriter(new BufferedWriter(new FileWriter(fname)));
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @Override
    public  void    write( String dataStream )
    {
        itsPrintWriter.printf("%s", dataStream );
    }   
    
    @Override
    public  void    close()
    {
        itsPrintWriter.close();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v06;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{

    public static void main(String[] args)
    {
        try
        {
            Account acc = new Account(null, 1);

            ScreenWriter sw = Writer.getScreenWriter();

            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(acc));
            System.out.println("");

            sw.write(FormatFactory.getFormatter(WriterMode.XML_WRITER).format(acc));
            System.out.println("");

            Customer cc = new Customer("Selvyn", "Wright", LocalDate.of(1965, 5, 8), "Birmingham, UK");

            sw.write(FormatFactory.getFormatter(WriterMode.JSON_WRITER).format(cc));

            System.out.println("");
        } catch (Exception ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

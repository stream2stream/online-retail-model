/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v12;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class SerializableFileReader implements InputChannel
{
    private String  itsFileName;
    private ObjectInputStream itsInputStream;
    
    public  SerializableFileReader()
    {
    }
    
    @Override
    public  void    openForReading( String fname )
    {
        itsFileName = fname;
        
        try
        {
            itsInputStream = new ObjectInputStream(new FileInputStream(fname));
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @Override
    public  Object    read()
    {
        Object result = null;
        try
        {
                result = itsInputStream.readObject();
        }catch (IOException |  ClassNotFoundException ex)
        {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            close();
        }
        return result;
    }   
    
    @Override
    public  void    close()
    {
        try
        {
            itsInputStream.close();
        } catch (IOException ex)
        {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}

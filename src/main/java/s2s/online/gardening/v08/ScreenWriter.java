/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v08;

/**
 *
 * @author Selvyn
 */
public class ScreenWriter   implements  OutputChannel
{
    @Override
    public  void    write( String dataStream )
    {
        System.out.printf("%s", dataStream );
    } 
    
    @Override
    public  void    close()
    {
    }

}

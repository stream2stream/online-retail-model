/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v10;

/**
 *
 * @author Selvyn
 */
public class Reader
{
    private static  final   TextFileReader  textFileReader = new TextFileReader();
    private static  final   SerializableFileReader  serializableReader = new SerializableFileReader();
    
    public  static  InputChannel    getTextFileReader( String fname )
    {
        textFileReader.openForReading(fname);
        
        return textFileReader;  
    }
    
    public  static  InputChannel    getSerializableFileReader( String fname )
    {
        serializableReader.openForReading(fname);
        
        return serializableReader;  
    }
}

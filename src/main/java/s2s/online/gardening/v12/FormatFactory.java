/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v12;

/**
 *
 * @author Selvyn
 */
public class FormatFactory
{
    private static  JSONFormatter   jsonfmt = new JSONFormatter();
    private static  XMLFormatter    xmlfmt = new XMLFormatter();
    
    public  static  StreamFormatter   getFormatter( WriterMode mode )
    {
        StreamFormatter result = jsonfmt;
        
        if( mode == WriterMode.XML_WRITER )
            result = xmlfmt;
        
        return result;
    }     
}

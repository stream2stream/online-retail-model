/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v07;

import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class Product
{
    private String  description;
    private double  price;
    private String  product_code;
    private int     quantity;
    private ArrayList items = new ArrayList();

    public  Product()
    {
        
    }
    
    public Product(String description, double price, String product_code, int qty)
    {
        this.description = description;
        this.price = price;
        this.product_code = product_code;
        this.quantity = qty;
    }
    
    public  void    addItem( Item newItem )
    {
        items.add(newItem);
    }

    public  Item    getItem( int itemId )
    {
        Item result = null;
        for( Object obj : items )
        {
            result = (Item)obj;
            if( result.getUniqueId() == itemId )
                break;
        }
        return result;
    }
    
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public String getProductCode()
    {
        return product_code;
    }

    public void setProductCode(String product_code)
    {
        this.product_code = product_code;
    }
    
    public  int getQuantity()
    {
        return quantity;
    }
    
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

/**
 *
 * @author Selvyn
 */
public class JSONFormatter  implements StreamFormatter
{
    private StringBuilder   formattedStream;

    @Override
    public  String  format( Account acc )
    {
        formattedStream = new StringBuilder();
        
        formattedStream.append("\"{Account\":[\n\t{\"Date Created\":\""); 
        formattedStream.append(acc.getDateCreated().toString());
        formattedStream.append("\"}\n]}");
        
        return formattedStream.toString();
    }
    
    @Override
    public  String    format( Customer cc )
    {
        formattedStream = new StringBuilder();
        
        formattedStream.append("\"Customer\":[\n\t{\"First Name\":\"");
        formattedStream.append(cc.getFname());
        formattedStream.append("\"},");
        formattedStream.append("\n\t{\"Last Name\":\"");
        formattedStream.append(cc.getLname());
        formattedStream.append("\"},");
        formattedStream.append("\n\t{\"Date of Birth\":\"");
        formattedStream.append(cc.getDob().toString());
        formattedStream.append("\"},");
        formattedStream.append("\n\t{\"Address\":\"");
        formattedStream.append(cc.getAddress());
        formattedStream.append("\"}");
        formattedStream.append("\n]}");
        
        return formattedStream.toString();
    }

}

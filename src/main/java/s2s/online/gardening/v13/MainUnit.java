/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v13;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
    public static void main(String[] args)
    {
        MainUnit munit = new MainUnit();
        ShutdownHandler sdh = new ShutdownHandler();
        
        KeyboardHandler kh = KeyboardHandler.getInstance();
        
        kh.addCommand("save", ()->{
            System.out.println("Save command called");
            munit.test_serialization_writer_all_objects();
        });
        
        kh.addCommand("load", ()->{
            System.out.println("Load command called");
            munit.test_serialization_reader_all_objects();
        });

        kh.addCommand("create", ()->{
            System.out.println("Create command called");
            munit.bootstrap_products_items();
            munit.bootstrap_customers_accounts();
            munit.bootstrap_orders_accounts_items();
        });
        
        // This sdhould eb the last command we call as it is a blocking call
        sdh.waitForShutdownCommand();
    }

    public void bootstrap_products_items()
    {
        // This call will bootstrap the products
        Warehouse.getInstance();
    }
    
    public  void    bootstrap_customers_accounts()
    {
        CustomerAccountMgr caMgr = CustomerAccountMgr.getInstance();
        // Create the accounts first and Customers
        Account acc = null;
        Customer cc = null;

        cc = new Customer("Sevlyn", "Wright", LocalDate.of(1965, 5, 8), "Somewhere in Birmingham");
        caMgr.addCustomer("C1", cc);
        acc = new Account(cc, 1);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);

        cc = new Customer("Samuel", "Wright", LocalDate.of(1982, 4, 22), "Somewhere else in Birmingham");
        caMgr.addCustomer("C2", cc);
        acc = new Account(cc, 2);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);

        cc = new Customer("Graham", "Meaden", LocalDate.of(1967, 4, 3), "Somewhere down South");
        caMgr.addCustomer("C3", cc);
        acc = new Account(cc, 3);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);
    }

    /*
     * This method will use an Order object to link the Item and Product objects 
     * to the Customer and Account objects
    */
    public  void    bootstrap_orders_accounts_items()
    {
        // Get an account and a product
        Account acc = CustomerAccountMgr.getInstance().getAccount(1);
        Product pp = Warehouse.getInstance().getProduct("JDF-001");
        
        // only proceed if the keys were valid
        if( acc != null && pp != null )
        {
            try 
            {
                // create new order giving it the order number of 1
                Order oo = new Order(acc, 1, LocalDate.now());
                
                // add the order the account
                acc.addOrder(oo);
                
                // Use the product to purchase X times the Product
                Item itm = pp.purchaseItem(3);
                
                // Only proceed if you used a valid item id
                if( itm != null )
                    oo.addItem(itm);  // this call decrease the stock for this product
            } catch (Exception ex) {
                Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void test_serialization_writer_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";

        // working with a Serializable File Writer
        SerializableFileWriter sfw = Writer.getSerializableFileWriter(fname);

        // Write the object out
        // HashMap are Serializable, so it can be written straight out
        // Because all objects in the HashMap are Serializable, they will all be
        // written out as well
        sfw.write(CustomerAccountMgr.getInstance().getCustomers());

        sfw.close();
    }

    public void test_serialization_reader_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";
        // Read the object from the filesystem
        InputChannel sfr = Reader.getSerializableFileReader(fname);
        sfr.openForReading(fname);

        // Read the entire object network back in
        HashMap< String, Customer> customersIn = (HashMap< String, Customer>) sfr.read();

        // Only process the objects if the read was successful
        if(customersIn != null)
        {
            // Iterate over all the Customers...
            for (Map.Entry<String, Customer> entry : customersIn.entrySet())
            {
                Customer cc = entry.getValue();
                
                System.out.printf("Customer code ==> %s\n", entry.getKey());
                cc.getRenderer().display();
                cc.getAccount().getRenderer().display();
                
                displayOrderDetais( cc.getAccount() );
            }
        }
    }
    
    public  void   displayOrderDetais( Account acc )
    {
        HashMap< Integer, Order > orders = acc.getOrders();
        for(Map.Entry<Integer, Order> entry : orders.entrySet())
        {
            Order ord = entry.getValue();
            BusinessObjectRenderer handler = new BusinessObjectRenderer()
            {
                @Override
                public void display()
                {
                    System.out.printf("Order\tORDER NO\tTOTAL COSR\tORDER DATE\n");
                    System.out.printf("\t%d\t\t%.2f\t\t%s\n", 
                                                ord.getOrderNo(), 
                                                ord.getTotalCost(), 
                                                ord.getDateCreated().toString());
                }
            
            };
            handler.display();
            
            displayItemDetails( ord );
        }
    }
    
    public  void    displayItemDetails( Order ord )
    {
        for( Item itm : ord.getItems())
        {
            BusinessObjectRenderer handler = new BusinessObjectRenderer()
            {
                @Override
                public void display()
                {
                    System.out.printf("Item\tUNIQUE ID\tQUANTITY\tCOST\n");
                    System.out.printf("\t%d\t\t%d\t\t%.2f\n", 
                                                itm.getUniqueId(),
                                                itm.getQuantity(),
                                                itm.getItemCost());
                }
            };
            handler.display();
            
            displayProductDetails(itm.getProduct());
        }
    }
    
    public  void    displayProductDetails( Product pp )
    {
        BusinessObjectRenderer handler = () ->
        {
            System.out.println("Product\tPRODUCT CODE\tDESCRIPTION\t\tPRICE\tQUANTITY");
            System.out.printf("\t%s\t\t%s\t\t%.2f\t%d\n", 
                    pp.getProductCode(), 
                    pp.getDescription(), 
                    pp.getPrice(), 
                    pp.getQuantity());
        };
        handler.display();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v11;

/**
 *
 * @author Selvyn
 */
@FunctionalInterface
public interface CommandExecutor
{
    public  void    execute();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v03;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class Order
{
    private int order_no;
    private double total_cost;
    private LocalDate order_date;
    private ArrayList items = new ArrayList(1);
    private Account itsAccount;

    public Order(Account acc, int orderNo, LocalDate orderDate)
    {
        this.itsAccount = acc;
        this.order_no = orderNo;
        this.order_date = orderDate;
    }

    public  Account getAccount()
    {
        return itsAccount;
    }

    public void addItem(Item anItem)
    {
        items.add(anItem);
    }

    public int getOrderNo()
    {
        return order_no;
    }

    public void setOrderNo(int orderno)
    {
        order_no = orderno;
    }

    public LocalDate getDateCreated()
    {
        return order_date;
    }

    public void setDateCreated(LocalDate date)
    {
        order_date = date;
    }

    public void setDateCreated(String date)
    {
        order_date = LocalDate.parse(date);
    }

    public double getTotalCost()
    {
        double result = 0.0;

        for (Object obj : items)
        {
            Item theItem = (Item) obj;
            result += theItem.getTotalCost();
        }
        return result;
    }
}
